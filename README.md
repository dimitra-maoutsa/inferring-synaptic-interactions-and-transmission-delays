# Inferring synaptic interactions and transmission delays



[under construction - more details soon - brief info on the poster I presented at BCCN 2016 [here](https://gitlab.com/di.ma/inferring-synaptic-interactions-and-transmission-delays/-/blob/master/poster-connectomics_through_nonlinear-1.png) and (only for the synaptic inference part) see the published article as pdf [here](https://gitlab.com/di.ma/Connectivity_from_event_timing_patterns/-/blob/master/PhysRevLett.121.054101.pdf) or directly from the publisher [here](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.121.054101) ]

<img src="reconstruction_quality_vs_CV.png"  width="30%" height="30%"><img src="required_event_scaling.png"  width="30%" height="30%"><img src="delay_misestimation.png" alt="influence of delay misestimation by dimitra maoutsa" width="30%" height="30%">

<img src="delay_misetimation_per_type.png"  width="30%" height="30%">
<img src="delay_error_landscape.png" alt="delay error landscape by dimitra maoutsa" width="30%" height="30%">
<img src="inferring_delays.png"  width="30%" height="30%">



